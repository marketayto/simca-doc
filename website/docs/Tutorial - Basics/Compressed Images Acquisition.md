---
sidebar_position: 5
---

# Feature D : Acquisition

The Acquisition tab is used to generate compressed measurements given :  a **scene** and a **filtering cube**.

Note that the scene is :
- cropped in the spatial dimensions to fit the filtering cube sampling (detector dimensions). 
- interpolated in the spectral dimension according to the filtering cube sampling (number of spectral samples).

![layout scene tab](/img/acquisition_tab.svg "Mask design tab")

## 1 - Settings

For now, it only includes one mode : single acquisition.

## 2 - Run Acquisition button

By clicking on this button :
- First, the scene cube is cropped in the spatial dimensions and interpolated in the spectral dimension.
- Second, a point by point multiplication is performed between the filtering cube and the reinterpolated scene. 

## 3 - Display measurements

### compressed measurements

The image as measured by the detector.

![layout scene tab](/img/layout_acquisition_1.svg "Mask design tab")

### Spectral images

Each slice of the filtered scene.

![layout scene tab](/img/layout_acquisition_2.svg "Mask design tab")

### Panchromatic image

No spatial/spectral filtering, the interpolated scene is simply summed along its spectral dimension


![layout scene tab](/img/layout_acquisition_3.svg "Mask design tab")